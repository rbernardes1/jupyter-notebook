# jupyter-notebook

## Description

This project reads a .json file and manipulates using `PySpark`.

## What is necessary to run this project
To run this project it is necessary to have `Python`, `Git`, `PySpark` and `Jupyter notebook` installed in your machine.

- The download of Git is available on this [link](https://git-scm.com/downloads).

- The download of Python is available on this [link](https://www.python.org/downloads/). 

- You can install PySpark using `PyPI` with the command

`pip install pyspark`

- You can install Jupyter notebook using `PyPI` with the command 

`pip install notebook`

- To access the project and clone through `cmd`

`git clone https://gitlab.com/rbernardes1/jupyter-notebook.git`


## Authors
- [**Renata Bernardes**](https://gitlab.com/rbernardes1)

